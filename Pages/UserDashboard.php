<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
	</script>
	
	
<script>
$(document).ready(function(){
    $("#bLogout").click(function(){
        $.post( "../Server/GlobalActions/LogOut.php", null, function(data){
        	window.location.href = '../index.php';});
    });

    $("#dMyData").click(function(){
        $.post( "../Server/GlobalActions/LogOut.php", null, function(data){
        	window.location.href = '../index.php';});
    });

    $("#dRent").click(function(){
        var req = new Object();
        req['mod'] = 'getAllBikes';
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
        	
        var t = $("<table>");
        for(var q in data){
        	var row = $("<tr>").appendTo(t);
            for(var f in data[q]){
            	$("<td>").
				html(data[q][f]).
				appendTo(row);
                console.log(data[q][f]);
            }
        }
        $("#dContent").html(t);
        
		    },"json")
    });
    
});
</script>
</head>
<body>
	<div>
		<p>Jesteś zalogowany jako: <?php session_start(); echo $_SESSION['mail'] ?></p>
		<button id="bLogout">Wyloguj się</button>
	</div>
	
	<div id = "dActions">
		<div id = "dMainPage">Strona główna</div>
		<div id = "dRent">Wypożycz rower</div>
		<div id = "dAddMoney">Doładuj konto</div>
		<div id = "dHistory">Historia wypożyczeń</div>
		<div id = "dMyData">Moje dane</div>
	</div>
	<div id = "dContent">
		
	</div>

</body>
</html>