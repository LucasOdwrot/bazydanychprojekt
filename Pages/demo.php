<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script>
<script>
$(document).ready(function(){
    $("#b1").click(function(){
        var req = new Object();
        req['mod'] = 'getAllBikes';
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b2").click(function(){
        var req = new Object();
        req['mod'] = 'getAllBikeModels';
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b3").click(function(){
        var req = new Object();
        req['mod'] = 'insertNewBike';

        var data = new Object();
        data['technical_state'] = "dobry";
        data['color'] = "red";
        data['availability'] = 1;
        data['model_id'] = 1;
        data['rent_count'] = 0;

        req['data'] = data;

        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b31").click(function(){
        var req = new Object();
        req['mod'] = 'getBikesAndModels';

        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b4").click(function(){
        var req = new Object();
        req['mod'] = 'registerSession';

        var data = new Object();
        data['email'] = "someMail@some.pl";
        data['id'] = "718287";
        data['userType'] = "customer";
        req['data'] = data;

        
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b5").click(function(){
    	 var req = new Object();
         req['mod'] = 'getSessionData';
         $.post( "../Server/clientReq.php", req, function(data){
         	console.log(data);
 		    },"json")
	});

    $("#b6").click(function(){
   	 var req = new Object();
        req['mod'] = 'terminateSession';
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b7").click(function(){
      	 var req = new Object();
           req['mod'] = 'getAllCustomers';
           $.post( "../Server/clientReq.php", req, function(data){
           	console.log(data);
   		    },"json")
   	});

    $("#b8").click(function(){
     	 var req = new Object();
          req['mod'] = 'getSingleCustomerByMail';

          req['email'] = 'someMail@some.pl';
          
          $.post( "../Server/clientReq.php", req, function(data){
          	console.log(data);
  		    },"json")
  	});
   
    $("#b9").click(function(){
   	 var req = new Object();
        req['mod'] = 'deleteCustomer';

        req['email'] = 'someMail@some.pl';
        
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});

    $("#b10").click(function(){
      	 var req = new Object();
           req['mod'] = 'getCustomerTransactionHistory';

           req['customerId'] = 8;
           
           $.post( "../Server/clientReq.php", req, function(data){
           	console.log(data);
   		    },"json")
   	});

    $("#b11").click(function(){
      	 var req = new Object();
           req['mod'] = 'getCustomerLoanHistory';

           req['customerId'] = 8;
           
           $.post( "../Server/clientReq.php", req, function(data){
           	console.log(data);
   		    },"json")
   	});

    $("#b12").click(function(){
     	 var req = new Object();
          req['mod'] = 'insertNewCustomer';

          var data = new Object();
          data['name'] = 'Arkadiusz';
          data['surname'] = 'Idzikowski';
          data['pesel'] = '95032802537';
          data['city'] = 'Sulechów';
          data['zip_code'] = '66-100';
          data['email'] = 'someMail@some.pl';
          data['password'] = 'password';
          data['street'] = 'SomeStreet';
          data['apartment_number'] = 5;
          data['phone_number'] = 5;

			//...all data
          req['data'] = data;
          
          $.post( "../Server/clientReq.php", req, function(data){
            debugger;
          	console.log(data);
  		    },"json")
  	});

    $("#b13").click(function(){
    	 var req = new Object();
         req['mod'] = 'rentBikeByCustomer';

         req['customerId'] = 8;
         req['bikeId'] = 2;
         
         $.post( "../Server/clientReq.php", req, function(data){
         	console.log(data);
 		    },"json")
 	});
 	
   $("#b14").click(function(){
   	 var req = new Object();
        req['mod'] = 'returnBike';

        req['customerId'] = 1;
        
        $.post( "../Server/clientReq.php", req, function(data){
        	console.log(data);
		    },"json")
	});


   $("#b15").click(function(){
	   	 var req = new Object();
	        req['mod'] = 'haveRent';

	        req['customerId'] = 1;
	        
	        $.post( "../Server/clientReq.php", req, function(data){
	        	console.log(data);
			    },"json")
		});

   $("#b16").click(function(){
	   	 var req = new Object();
	        req['mod'] = 'login';

	        req['email'] = 'someMail@some.pl';
	        req['password'] = 'password';
	        
	        $.post( "../Server/clientReq.php", req, function(data){
	        	console.log(data);
			    },"json")
		});

   $("#b17").click(function(){
	   	 var req = new Object();
	   	req['mod'] = 'giveBikeToClient';
	   	
	   	req['customerId'] = 8;
        req['employeeId'] = 1;
	        $.post( "../Server/clientReq.php", req, function(data){
	        	console.log(data);
			    },"json")
		});

   $("#b18").click(function(){
	   	 var req = new Object();

	        $.post( "../Server/clientReq.php", req, function(data){
	        	console.log(data);
			    },"json")
		});

   $("#b19").click(function(){
	   	 var req = new Object();
	        req['mod'] = 'getBikeFromClient';

	        req['customerId'] = 8;
	        
	        $.post( "../Server/clientReq.php", req, function(data){
	        	console.log(data);
			    },"json")
		});
		
});
</script>

<div>
	<p>Bike operations:</p>
	<button id = "b1">Show bikes</button>
	<button id = "b2">Show bike models</button>
	<button id = "b3">Insert some bike</button>
	<button id = "b31">GetBikesAndModels</button>
	<br/>
	<p>Session:</p>
	<button id = "b4">registerSession</button>
	<button id = "b5">getSessionData</button>
	<button id = "b6">terminateSession</button>
	<br/>
	<p>Customer:</p>
	<button id = "b7">getAllCustomers</button>
	<button id = "b8">getSingleCustomerByMail</button>
	<button id = "b9">deleteCustomer</button>
	<button id = "b10">getCustomerTransactionHistory</button>
	<button id = "b11">getCustomerLoanHistory</button>
	<button id = "b12">insertNewCustomer</button>
	<button id = "b13">rentBikeByCustomer</button>
	
	<button id = "b15">haveRent</button>
	<button id = "b16">login</button>
	<br/>
	<p>Employee:</p>
	<button id = "b17">giveBikeToCustomer</button>
<!-- 	<button id = "b18">insertNewEmployee</button> -->
	<button id = "b19">getBikeFromClient</button>
	<br/>
</div>


</body>
</html>