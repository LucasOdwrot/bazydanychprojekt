<!DOCTYPE html>
<html lang="en">
<head>
<title>Rejestracja</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/mycss.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<script>
// 	$(document).ready(function(){
// 		$("#bSubmit").click(function(){
// 			var req = {};
// 			$("input").each(function(){
// 				if($(this).val()==""){
// 					$("#dError").html("Nie wszystkie wymagane dane zostały podane!<br/>");
// 					return;
// 				}else 
// 					req[$(this).attr("name")]=$(this).val();
// 			});
// 			$.post( "../Server/SRegister.php", req, function( data ) {
// 				debugger;
// 				if(typeof data["errors"] !== 'undefined')
// 				{
// 					var errors = data['errors'];
// 					$('#dError').html(data[0]);
// 				}
// 				if(typeof data["link"] !== 'undefined'){
// 					window.location.href = '../index.php';
// 				}
// 			}, "json");
// 			debugger;
// 		});
// 	});
</script>

<script>
function changeSite(){
	window.location.href = "http://localhost/bazyDanychProjekt/pages/index.php";
	return false;
}

$(document).ready(function(){
	$("form").submit(function(){
		
		var req = new Object();
		req['mod'] = 'insertNewCustomer';
		
		var data = new Object();
		data['name'] = document.querySelector('[name="name"]').value;
		data['surname'] = document.querySelector('[name="surname"]').value;
		data['pesel'] = document.querySelector('[name="pesel"]').value;
		data['city'] = document.querySelector('[name="city"]').value;
		data['zip_code'] = document.querySelector('[name="zip_code"]').value;
		data['email'] = document.querySelector('[name="email"]').value;
		data['password'] = document.querySelector('[name="password"]').value;
			//...all data
		req['data'] = data;
		$.post( "../Server/clientReq.php", req, function(data){
			console.log(data);
		},"json");

	});
	
// 	$('bSubmit').click(function(){
// 		var req = new Object();
// 		req['mod'] = 'insertNewCustomer';
		
// 		var data = new Object();
// 		data['name'] = 'Arkadiusz';
// 		data['surname'] = 'Idzikowski';
// 		data['pesel'] = '95032802537';
// 		data['city'] = 'Sulechów';
// 		data['zip_code'] = '66-100';
// 		data['email'] = 'someMail@some.pl';
// 		data['password'] = 'password';
		
// 			//...all data
// 		req['data'] = data;
// 		$.post( "../Server/clientReq.php", req, function(data){
// 			console.log(data);
// 		},"json");
// 	});
	
});
</script>
</head>
<body>
	<nav class="navbar navbar-default" style="background: #d8dfe5;">
		<div class="container">
			<div class="box-content-reg">

				<!-- LOGO -->
				<a class="my-logo" href="http://localhost/bazyDanychProjekt/pages/mainpage.html"> <img src="css/logo.png" alt="Logo"
					style="width: 100px; height: 60px;">
				</a>

			</div>
		</div>
	</nav>

	<div class="container-fluid vertical-align myImage">
		<div class="row">
			<div class="col-md-offset-3 col-md-6 ">
				<form class="form-reg" style="background: rgba(216, 223, 229, 0.8);" onsubmit="return changeSite()">
				<strong class="line-thru text-uppercase" style="color: white;">Rejestracja</strong>
				
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">Imie</label>
						<input type="text" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="Imie" name="name">
					</div>
					
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">Nazwisko</label>
						<input type="text" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="Nazwisko" name="surname">
					</div>
					
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">Haslo</label>
						<input type="password" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="Haslo" name="password">
					</div>

					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">PESEL</label>
						<input type="text" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="PESEL" name="pesel">
					</div>
					
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">Miejscowosc</label>
						<input type="text" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="Miejscowosc" name="city">
					</div>
					
<!-- 					<div class="form-group"> -->
<!-- 						<label class="sr-only" for="exampleInputPassword1">Ulica</label> -->
<!-- 						<input type="text" class="form-control" -->
<!-- 							id="exampleInputPassword1" required="required" placeholder="Ulica"> -->
<!-- 					</div> -->
					
<!-- 					<div class="form-group"> -->
<!-- 						<label class="sr-only" for="exampleInputPassword1">Nr mieszkania</label> -->
<!-- 						<input type="text" class="form-control" -->
<!-- 							id="exampleInputPassword1" required="required" placeholder="Nr mieszkania"> -->
<!-- 					</div> -->
					
					<div class="form-group">
						<label class="sr-only" for="exampleInputPassword1">ZIP</label>
						<input type="text" class="form-control"
							id="exampleInputPassword1" required="required" placeholder="ZIP" name="zip_code">
					</div>
					

					<div class="form-group">
						<label class="sr-only" for="exampleInputEmail1">Email address</label>
						<input type="email" class="form-control" id="exampleInputEmail1"
							 required="required" placeholder="Email" name="email">
					</div>

<!-- 					<div class="form-group"> -->
<!-- 						<label class="sr-only" for="exampleInputPassword1">Nr telefonu</label> -->
<!-- 						<input type="text" class="form-control" -->
<!-- 							id="exampleInputPassword1" required="required" placeholder="Nr telefonu"> -->
<!-- 					</div> -->


					<button type="submit"
						class="btn btn-block btn-lg text-uppercase mySubmit" id="bSubmit">Zarejestruj
						sie</button>

					<p class="czy-konto">
						Masz juz konto? <a href="http://localhost/bazyDanychProjekt/pages/index.php">Zaloguj sie</a>
					</p>

				</form>
			</div>
		</div>

	</div>

	<!-- <div id="dError"></div> -->

	<!-- Name<br> -->
	<!-- <input type="text" name="name"/> -->
	<!-- <br> -->

	<!-- Surname<br> -->
	<!-- <input type="text" name="surname"/> -->
	<!-- <br> -->

	<!-- Password<br> -->
	<!-- <input type="password" name="password"/> -->
	<!-- <br> -->

	<!-- PESEL<br> -->
	<!-- <input type="text" name="pesel"/> -->
	<!-- <br> -->

	<!-- City<br> -->
	<!-- <input type="text" name="city"/> -->
	<!-- <br> -->

	<!-- Street<br> -->
	<!-- <input type="text" name="street"/> -->
	<!-- <br> -->

	<!-- Apartment Number<br> -->
	<!-- <input type="text" name="apartmentNumber"/> -->
	<!-- <br> -->

	<!-- ZIP<br> -->
	<!-- <input type="text" name="zip"/> -->
	<!-- <br> -->

	<!-- Mail<br> -->
	<!-- <input type="text" name="mail"/> -->
	<!-- <br> -->

	<!-- Phone number<br> -->
	<!-- <input type="text" name="number"/> -->
	<!-- <br> -->

	<!-- <br> -->

	<!-- <button id="bSubmit">Submit</button> -->

	<!--  	MOJE ALE Z�E -->
	<!-- 	<div id="myHeader"> -->
	<!-- <img src="css/logo.png" alt="Logo" style="width: 100px; height: 60px;"> -->
	<!-- 	</div> -->

	<!-- 	<div id="content"> -->



	<!-- 		<div id="dError"></div> -->

	<!-- 		<strong class="line-thru">REJESTRACJA</strong> -->

	<!-- 		<form> -->
	<!-- 			<ul> -->
	<!-- 				<li><input type="text" name="name" required="required" -->
	<!-- 					placeholder="Imie" /></li> -->
	<!-- 				<li><input type="text" name="surname" required="required" -->
	<!-- 					placeholder="Nazwisko" /></li> -->
	<!-- 				<li><input type="password" name="password" required="required" -->
	<!-- 					placeholder="Haslo" /></li> -->
	<!-- 				<li><input type="text" name="pesel" required="required" -->
	<!-- 					placeholder="Pesel" /></li> -->
	<!-- 				<li><input type="text" name="city" required="required" -->
	<!-- 					placeholder="Miejscowosc" /></li> -->
	<!-- 				<li><input type="text" name="street" required="required" -->
	<!-- 					placeholder="Ulica" /></li> -->
	<!-- 				<li><input type="text" name="apartmentNumber" required="required" -->
	<!-- 					placeholder="Nr mieszkania" /></li> -->
	<!-- 				<li><input type="text" name="zip" required="required" -->
	<!-- 					placeholder="ZIP" /></li> -->
	<!-- 				<li><input type="text" name="mail" required="required" -->
	<!-- 					placeholder="Adres e-mail" /></li> -->
	<!-- 				<li><input type="text" name="number" required="required" -->
	<!-- 					placeholder="Nr telefonu" /></li> -->
	<!-- 			</ul> -->

	<!-- 			<a href="" id="bSubmit" class="btn">ZAREJESTRUJ SIE</a> -->
	<!-- 			<button type="submit" id="bSubmit" class="btn">Zarejestruj Sie</button> -->
	<!-- 			<p class="czy-konto"> -->
	<!-- 				Masz juz konto? <a href="">Zaloguj sie</a> -->
	<!-- 			</p> -->

	<!-- 		</form> -->
	<!-- 	</div> -->

</body>
</html>