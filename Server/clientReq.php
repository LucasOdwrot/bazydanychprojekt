<?php
require_once dirname(dirname(__FILE__)).
					DIRECTORY_SEPARATOR.
					'Common'.DIRECTORY_SEPARATOR.
					'loader.php';
	
	//<----------------------------Session------------------------------->
	function registerSession(){
		$so = new SessionOperations();
		$so->registerSession($_POST['data']);
	}
	
	function getSessionData(){
		$so = new SessionOperations();
		echo json_encode($so->getSessionData());
	}
	
	function terminateSession(){
		$so = new SessionOperations();
		$so->terminateSession();
	}
	
	//<----------------------------Customer------------------------------->
	function getAllCustomers(){
		$co = new CustomerOperations();
		echo json_encode($co->getAllCustomers());
	}
	
	function getSingleCustomerByMail() {
		$co = new CustomerOperations();
		echo json_encode($co->getSingleCustomerByMail($_POST['email']));
	}
	
	function deleteCustomer() {
		$co = new CustomerOperations();
		echo json_encode($co->deleteCustomer($_POST['email']));
	}
	
	function getCustomerTransactionHistory(){
		$co = new CustomerOperations();
		echo json_encode($co->getCustomerTransactionHistory($_POST['customerId']));
	}
	
	function getCustomerLoanHistory(){
		$co = new CustomerOperations();
		echo json_encode($co->getCustomerLoanHistory($_POST['customerId']));
	}
	
	function insertNewCustomer(){
		$co = new CustomerOperations();
		echo json_encode($co->insertNewCustomer($_POST['data']));
	}
	
	function rentBikeByCustomer(){
		$co = new CustomerOperations();
		echo json_encode($co->rentBikeByCustomer($_POST['customerId'],$_POST['bikeId']));
	}
	
	function haveRent(){
		$co = new CustomerOperations();
		echo json_encode($co->haveRent($_POST['customerId']));
	}
	
	function login(){
		$co = new CustomerOperations();
		$resp = $co->login($_POST['email'],$_POST['password']);
		$ans;
		if($resp){
			$ans = $co->login($_POST['email'],$_POST['password']);
		}else{
			$eo = new EmployeeOperations();
			$ans = $eo->login($_POST['email'],$_POST['password']); 
		}
		
		echo json_encode($ans);
		
	}
	//<----------------------------Discounts------------------------------->
	function getAllDiscounts() {
		$do = new DiscountOperations();
		echo json_encode($do->getAllDiscounts()); 
	}
	
	function insertNewDiscount(){
		$do = new DiscountOperations();
		echo json_encode($do->insertNewDiscount($_POST['data']));
	}
	
	function deleteDiscount() {
		$do = new DiscountOperations();
		echo json_encode($do->deleteDiscount($_POST['id'])); 
	}
	//<----------------------------Employee------------------------------->
	function giveBikeToClient() {
		$eo = new EmployeeOperations();
		echo json_encode($eo->giveBikeToClient($_POST['customerId'], $_POST['employeeId']));
	}
	
	function insertNewEmployee() {
		$eo = new EmployeeOperations();
		echo json_encode($eo->insertNewEmployee($_POST['data']));
	}
	
	function getBikeFromClient(){
		$eo = new EmployeeOperations();
		echo json_encode($eo->getBikeFromClient($_POST['customerId']));
	}
	//<----------------------------Bikes------------------------------->
	
	/**
	 * retrun all available bikes
	 * 
	 */
	function getAllBikes(){
		$bo = new BikeOperations();
		echo json_encode($bo->getAvailableBikes());
	}
	
	function getBikesAndModels(){
		$bo = new BikeOperations();
		echo json_encode($bo->getBikesAndModels());
	}
	
	function getAllBikeModels(){
		$bo = new BikeOperations();
		echo json_encode($bo->getAvailableBikeModels());
	}
	
	function insertNewBike(){
		$bo = new BikeOperations();
		echo json_encode($bo->insertNewBike($_POST['data']));
	}
	
	/*!
	 * ***********************************************************
	 */
	
	if(function_exists($_POST['mod'])){
		call_user_func($_POST['mod']);
	}
?>