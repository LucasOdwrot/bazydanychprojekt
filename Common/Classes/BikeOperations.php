<?php
require_once dirname ( dirname ( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'loader.php';
class BikeOperations {
	
	
	public function getAvailableBikes() {
		$query = "SELECT * FROM bikes WHERE availability = '1' 
					ORDER BY id";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function getAvailableBikeModels(){
		$query = "SELECT * FROM bike_models";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function getBikesAndModels(){
		$query = "SELECT * FROM discounted_bikes_view";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	
	public function insertNewBikeModel($data){
		$brand = $data['brand'];
		$model = $data['model'];
		$weight = $data['weight'];
		$price_per_hour = $data['price_per_hour'];
		$type =  $data['type'];
		
		$query = "INSERT INTO bike_models (brand, model, weight, price_per_hour)
		VALUES ('$brand', '$model', '$weight', '$price_per_hour', '$type')";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function insertNewBike($data){
		$tState = $data['technical_state'];
		$color = $data['color'];
		$available = $data['availability'];
		$model = $data['model_id'];
		$rent_count = $data['rent_count'];
		
		$query = "INSERT INTO bikes  (technical_state, color, availability, model_id, rent_count)
		VALUES ('$tState', '$color', '$available', '$model', '$rent_count')";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function deleteBike($id){
		$query = "DELETE FROM bikes
		WHERE id = $id";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function  modifyBikeState($bikeId, $newState){
		$query = "UPDATE bikes
		SET technical_state='$newState'
		WHERE id=$bikeId";
	}
	
	public function  modifyBikeAvailability($bikeId, $availibility){
		$query = "UPDATE bikes
		SET availability='$availibility'
		WHERE id=$bikeId";
	}
}
?>