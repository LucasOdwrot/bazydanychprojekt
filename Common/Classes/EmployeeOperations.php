<?php
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'loader.php';

class EmployeeOperations{
	public function getAllEmployees() {
		$query = "SELECT * FROM customers ORDER BY id";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function insertNewEmployee($data){
		$pesel = $data['pesel'];
		$name = $data['name'];
		$surname = $data['surname'];
		$city = $data['city'];
		$street = $data['street'];
		$apartment_number = $data['apartment_number'];
		$email = $data['email'];
		$password = $data['password'];
		$phone_number = $data['phone_number'];
		$boss_id = $data['boss_id'];
		
	
		$password = password_hash($password, PASSWORD_DEFAULT);
	
		$query = "INSERT INTO employees (pesel, name, surname, city, street, apartment_number, email, password, phone_number, boss_id)
		VALUES ('$pesel', '$name', '$surname', '$city', '$street','$apartment_number','$email','$password','$phone_number', $boss_id)";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function giveBikeToClient($customerId, $employeeId){
		$query = "SELECT id FROM loan_history WHERE
		customer_id = '$customerId'
		AND employee_id IS NULL";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		if(empty($resp['result'])){
			$resp['error'] = 'Cleint have no reservations';
			return $resp;
		}
		
		$loan_id = $resp['result'][0]['id'];
		$currentDate = date("Y-m-d H:i:s", time());
		
		$query = "UPDATE loan_history
		SET rent_start='$currentDate',
			employee_id = '$employeeId'
		WHERE id=$loan_id";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function getBikeFromClient($customer_id){
		$query = "SELECT id FROM loan_history WHERE customer_id = '$customer_id' AND rent_end IS NULL AND rent_start IS NOT NULL";
			
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		if(empty($resp['result'])){
			$resp['error'] = 'There is no bike to return';
			return $resp;
		}
	
		$loan_id = $resp['result'][0]['id'];
		$currentDate = date("Y-m-d H:i:s", time());
	
		$query = "UPDATE loan_history
		SET rent_end='$currentDate'
		WHERE id=$loan_id";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function login($email, $password){
		$hashPass = hash('sha512',$password);
		$query = "SELECT id FROM EMPLOYEES WHERE email = '$email' AND password = '$hashPass'";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		if(count($resp['result'])>0){
			$so = new SessionOperations();
			$so->createSession($email, 0);
				
			return true;
		}
		else return false;
	}
	
}
?>