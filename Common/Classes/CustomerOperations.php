<?php
require_once dirname ( dirname ( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'loader.php';
class CustomerOperations {
	public function getAllCustomers() {
		$query = "SELECT * FROM customers ORDER BY id";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function getSingleCustomerByMail($email) {
		$query = "SELECT * FROM customers WHERE email = '$email'";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function deleteCustomer($email) {
		$query = "DELETE FROM customers WHERE email = '$email'";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function getCustomerTransactionHistory($userId){
		$query = "SELECT * FROM transaction_history WHERE customer_id = '$userId'";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function getCustomerLoanHistory($customerId){
		$query = "SELECT * FROM loan_history WHERE customer_id = '$customerId' ORDER BY 'id'";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function insertNewCustomer($data){
		$pesel = $data['pesel'];
		$name = $data['name'];
		$surname = $data['surname'];
		$city = $data['city'];
		$street = $data['street'];
		$apartment_number = $data['apartment_number'];
		$email = $data['email'];
		$password = $data['password'];
		$phone_number = $data['phone_number'];
		
		$hashPass = hash('sha512',$password);
		
		$query = "INSERT INTO customers (pesel, name, surname, city, street, apartment_number, email, password, phone_number)
		VALUES ('$pesel', '$name', '$surname', '$city', '$street','$apartment_number','$email','$hashPass','$phone_number')";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	//add trigers
	public function rentBikeByCustomer($userId, $bikeId){
		$query = "INSERT INTO loan_history(customer_id, bike_id) VALUES ('$userId','$bikeId')";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	
	public function haveRent($customer_id){
		$query = "SELECT id FROM loan_history WHERE customer_id = '$customer_id' AND rent_end IS NULL";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return empty(!$resp['result']);
	}
	
	public function login($email, $password){
		$hashPass = hash('sha512',$password);
		$query = "SELECT id FROM CUSTOMERS WHERE email = '$email' AND password = '$hashPass'";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		if(count($resp['result'])>0){
			$so = new SessionOperations();
			$so->createSession($email, 1);
			
			return true;
		}
		else return false;
	}
}
?>