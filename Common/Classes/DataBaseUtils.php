<?php
require_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'loader.php';
class DataBaseUtils {
	private $dbHandler;
	private $errorString;
	private $errorNum;
	
	private $HOST = "localhost";
	private $USER = "root";
	private $PASSWORD = "";
	private $DATABASE_NAME = "bicyclerental";
	
	public function __construct() {
	}
	/**
	 * 
	 * @param MySQL query string $_query
	 */
	public function executeQuery($_query) {
		$this->openConnection ();
		$result = mysqli_query ( $this->dbHandler, $_query );
		
		$this->errorString = mysqli_error ( $this->dbHandler );
		$this->errorNum = mysqli_errno ( $this->dbHandler );
		
		$resp = Array();
		
		$resp['result'] = $this->getAssocRespData($result);
		$resp['errorNumber'] = $this->errorNum;
		$resp['error'] = $this->errorString;
		
		$this->closeConnection ();

		return $resp;
	}
	
	private function openConnection() {
		$dbHandler = mysqli_connect ( $this->HOST, $this->USER, $this->PASSWORD );
		mysqli_select_db ( $dbHandler, $this->DATABASE_NAME);
		mysqli_query ( $dbHandler, "SET CHARSET `utf8`" );
		mysqli_query ( $dbHandler, "SET NAMES `utf8` COLLATE `utf8_bin`" );
		$this->dbHandler = $dbHandler;
	}
	
	private function closeConnection() {
		mysqli_close ( $this->dbHandler );
	}
	
	private function getAssocRespData($_res) {
		$data = Array ();
		
		if ($_res instanceof mysqli_result) {
			while ( $row = mysqli_fetch_assoc ( $_res ) ) {
				foreach ( $row as $key => $pos ){
					$row [$key] = is_null ( $pos ) ? "" : $pos;
				}
				$data [] = $row;
			}
			
			mysqli_free_result ( $_res );
		}
		
		return $data;
	}
}
?>