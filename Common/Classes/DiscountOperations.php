<?php
require_once dirname ( dirname ( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'loader.php';
class DiscountOperations {
	public function getAllDiscounts() {
		$query = "SELECT * FROM Discount";
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		return $resp;
	}
	
	public function insertNewDiscount($data){
		$modelId= $data['bike_model_id'];
		$value = $data['value'];
	
		$query = "INSERT INTO Discount (bike_model_id, value)
		VALUES ('$modelId', '$value')";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
	
	public function deleteDiscount($id) {
		$query = "DELETE FROM Discount WHERE id = $id";
	
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
	
		return $resp;
	}
}
?>