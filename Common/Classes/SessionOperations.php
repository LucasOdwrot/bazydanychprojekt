<?php
require_once dirname ( dirname ( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'loader.php';

class SessionOperations{
	public function  registerSession($data){
		terminateSession();
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		$_SESSION['id'] = $data['id'];
		$_SESSION['email'] = $data['email'];
		$_SESSION['userType'] = $data['userType'];
	}
	
	public function getSessionData(){
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		return $_SESSION;
	}
	
	public function terminateSession(){
		if (session_status() == PHP_SESSION_NONE)
			session_start();
		session_destroy();
	}
	
	public function createSession($email, $type){
		if($type == 1){
			$query = "SELECT * FROM customers WHERE email = '$email'";
			$data['userType'] = 'client';
		}else{
			$query = "SELECT * FROM employees WHERE email = '$email'";
			$data['userType'] = 'employee';
		}
		
		
		$db = new DataBaseUtils ();
		$resp = $db->executeQuery ( $query );
		
		$user = $resp['result'][0];
		$data['id'] = $user['id'];
		$data['email'] = $user['email'];
		
		
		$this->registerSession($data);
	}
}
?>